﻿using System;
using System.IO;
using System.Globalization;
using System.Collections.Generic;

namespace JoseBenitezP6M03UF5
{
    /// <summary>
    /// Les línies de codi comentades són les proves d'excepcions per a provar abans tot allò que va bé
    /// Descomenta les linies per comprovar l'us de les excepcions
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Joc de proves de la funció divideixoCero amb: 7 i 2");
            int divideixoCeroprova1 = divideixoCero(7, 2);
            Console.WriteLine(divideixoCeroprova1);

            Console.WriteLine("Joc de proves de la funció divideixoCero amb: 8 i 4");
            int divideixoCeroprova2 = divideixoCero(8, 4);
            Console.WriteLine(divideixoCeroprova2);

            Console.WriteLine("Per testejar l'excepció, descomenta de la linia 25 a la 27");
            //Console.WriteLine("Joc de proves de la funció divideixoCero amb: 5 i 0");
            //int divideixoCeroprova3 = divideixoCero(5, 0);
            //Console.WriteLine(divideixoCeroprova3);


            Console.WriteLine("Joc de proves de la funció aDoubleoU amb:  7,1");
            Console.WriteLine("   " + aDoubleoU("7.1"));

            Console.WriteLine("Joc de proves de la funció aDoubleoU amb:  9.");
            Console.WriteLine("   " + aDoubleoU("9."));

            Console.WriteLine("Joc de proves de la funció aDoubleoU amb:  .2");
            Console.WriteLine("   " + aDoubleoU(".2"));

            Console.WriteLine("Joc de proves de la funció aDoubleoU amb:  tres");
            Console.WriteLine("   " + aDoubleoU("tres"));


            Console.WriteLine("En aquest apartat has de donar una ruta d'arxiu al mètode LlegirArxiu");
            string arxiu = "";
            //LlegirArxiu(arxiu);

            Console.WriteLine("L'aplicació que he creat es tracta d'una classe genèrica on agafo dos tipus de dades Int i String per fer proves");
            Console.WriteLine("Creo la següent llista de Int amb el mètode d'afegir un nou element anomenat AddElement(T element)");
            Llista<int> llistaint = new Llista<int>();
            llistaint.AddElement(1);
            llistaint.AddElement(2);
            llistaint.AddElement(3);
            Console.WriteLine(llistaint.ToString());
            Console.WriteLine("Li trec l'element de la posició 1 amb el mètode RemoveElement(int posició) de tal manera que queda així:");
            llistaint.RemoveElement(1);
            Console.WriteLine(llistaint.ToString());

            Console.WriteLine("Creo la següent llista de String");
            Llista<string> llistastring = new Llista<string>();
            llistastring.AddElement("hello");
            llistastring.AddElement("world");

            Console.WriteLine(llistastring.ToString());


            Console.WriteLine("Li trec l'element de la posició 0");
            llistastring.RemoveElement(0);
            Console.WriteLine(llistastring.ToString());

            Console.WriteLine("Ara intento treure un element en una posició inexistent al llistat string");
            llistastring.RemoveElement(4);
            Console.WriteLine(llistastring.ToString());

            Console.WriteLine("L'excepció de ArgumentNullException no l'he pogut implementar o més ben dit, posar a prova ja que de per si el int no me'ls deixa posar i els strings nulls passen i s'afegeixen sense cap problema. Això també em passa amb altres tipus");


        }


        static int divideixoCero(int dividend, int divisor)
        {
            try
            {
                // Intentem fer la divisió
                return dividend / divisor;
            }
            catch (DivideByZeroException)
            {
                // Si el denominador és 0, llancem una excepció ArithmeticException
                throw new DivideByZeroException();
            }
        }


        public static double aDoubleoU(string s)
        {
            double result = 1.0;

            try
            {
                //Aqui hi ha un intent de posar el punt pero no hi havia manera, sempre em posava la coma
                result = double.Parse(s, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                Console.WriteLine("Error: El valor introduit no és vàlid.");
            }

            return result;
        }

        public static void LlegirArxiu(string arxiu)
        {
            try
            {
                // Intentem llegir l'arxiu
                string contingut = File.ReadAllText(arxiu);
                Console.WriteLine(contingut);
            }
            catch (IOException ex)
            {
                // Si hi ha un error mostrem el misstage d'error
                Console.WriteLine($"S'ha produït un error d'entrada/sortida {ex.Message}");
            }
        }
    }
}
