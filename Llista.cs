﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoseBenitezP6M03UF5
{
    //Classe Llista que bàsicament és un objecte de tipus List<T>
    public class Llista<T>
    {
        private List<T> llista;

        public Llista()
        {
            llista = new List<T>();
        }

        /// <summary>
        /// Mètode per a afegir un nou element a la llista
        /// </summary>
        /// <param name="element">Qualsevol tipus d'objecte</param>
        public void AddElement(T element)
        {
            try
            {
                llista.Add(element);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("Error: Argument is null. " + ex.Message);
            }
        }

        /// <summary>
        /// Mètode per a borrar l'item d'una posició en concret
        /// </summary>
        /// <param name="index">Posició de l'ítem que es vol esborrar</param>
        public void RemoveElement(int index)
        {
            try
            {
                llista.RemoveAt(index);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Error: Index is out of range. " + ex.Message);
            }
        }

        /// <summary>
        /// Mètode que retorna la llista sencera com a un sol string
        /// </summary>
        /// <returns>Conjunt dels elements passats a string</returns>
        public override string ToString()
        {
            string _llista = "";
            foreach (var element in llista)
            {
                _llista += element + "\n";
            }
            return _llista;
        }
    }


}
